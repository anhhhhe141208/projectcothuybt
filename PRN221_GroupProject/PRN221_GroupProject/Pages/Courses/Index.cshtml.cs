using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.Courses
{
    public class IndexModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IndexModel(PRN221Context context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }
        public List<Course> Courses { get; set; }

        public IActionResult OnGet(int index)
        {
            if(index == 0)
            {
                index = 1;
            }
         var course = _context.Courses.Skip((index-1)*5).Take(5).ToList();
            int totalcourse = _context.Courses.Count();
            int endpage = totalcourse / 5;
            if(totalcourse % 5 != 0)
            {
                endpage++;
            }
            var session = _httpContextAccessor.HttpContext.Session;
            session.SetInt32("endpageCourse", endpage);
            Courses = course;
            return Page();
        }
    }
}
