using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.Rate
{
    public class RatingModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public RatingModel(PRN221Context context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }
        // public User User { get; set; }
        public List<Rating> Rating { get; set; }

        public int Count = 0;

        public IActionResult OnGet()
        {
            var session = _httpContextAccessor.HttpContext.Session;
   
            Rating = _context.Ratings.Include(o => o.User).Include(o => o.Course).Where(x=>x.CourseId == session.GetInt32("coursedetailId")).ToList();
             var CourseDetail = _context.Courses.Where(x => x.CourseId == session.GetInt32("coursedetailId")).FirstOrDefault();
            var check = _context.MyCourses.Include(x => x.Course).Where(x => x.UserId == session.GetInt32("userid")).Select(x => x.Course.CourseName).ToList();
            if (check != null)
            {
                foreach (var item in check)
                {
                    if (item.Equals(CourseDetail.CourseName))
                    {
                        Count++;
                    }

                }
            }
            return Page();
        }

        public void OnPost(string message)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            var rating = new Rating();
            rating.CourseId = (int)session.GetInt32("coursedetailId");
            rating.UserId = (int)session.GetInt32("userid");
            rating.Comment = message;
            _context.Ratings.Add(rating);
            _context.SaveChanges();
            Rating = _context.Ratings.Include(o => o.User).Include(o => o.Course).Where(x => x.CourseId == session.GetInt32("coursedetailId")).ToList();

        }

    }
}
