using PRN221_GroupProject.Pages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.Users
{
    public class IndexModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IndexModel(PRN221Context context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }
        public List<User> Users { get; set; }

        public async Task OnGetAsync(int index)
        {
            if(index ==0)
            {
                index = 1;
            }
            Users = _context.Users.Skip((index-1)*5).Take(5).ToList();
            int totaluser = _context.Users.Count();
            int endpage = totaluser / 5;
            if(totaluser % 5 != 0)
            {
                endpage++;
            }
            var session = _httpContextAccessor.HttpContext.Session;
            session.SetInt32("endpageUser", endpage);
            //IQueryable<User> user = from s in _context.Users
            //                        select s;
            //var pageSize = _configuration.GetValue("PageSize", 3);

            //Users = await PaginatedList<User>.CreateAsync(user.AsNoTracking(), pageNum ?? 1, pageSize);
        }

    }
}
