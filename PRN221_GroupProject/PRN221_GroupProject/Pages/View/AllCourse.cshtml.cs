using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.View
{
    public class AllCourseModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AllCourseModel(PRN221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }
        [BindProperty]
        public List<Course> AllCourse { get; set; }
        public void OnGet(int index)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            if (index == 0)
            {
                index = 1;
            }
            AllCourse = _context.Courses.Skip((index-1)*6).Take(6).ToList();
            int totalCourse = _context.Courses.Count();
            int endpage = totalCourse / 6;
            if(totalCourse % 6 !=0)
            {
                endpage++;
            }
            session.SetInt32("CourseEndPage", endpage);


        }
    }
}
