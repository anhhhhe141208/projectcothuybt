using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.View
{
    public class CourseDetailModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CourseDetailModel(PRN221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        [BindProperty]
        public Course CourseDetail { get; set; }
        public int Count = 0;
        public IActionResult OnGet(int id)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            if(id == 0)
            {
                return NotFound();
            }
            CourseDetail = _context.Courses.Where(x => x.CourseId == id).FirstOrDefault();
            var check = _context.MyCourses.Include(x => x.Course).Where(x => x.UserId == session.GetInt32("userid")).Select(x=>x.Course.CourseName).ToList();
            if (check != null)
            {
                foreach (var item in check)
                {
                    if (item.Equals(CourseDetail.CourseName))
                    {
                        Count++;
                    }

                }
            }
            session.SetInt32("coursedetailId",id);
           
            return Page();
        }
    }
}
