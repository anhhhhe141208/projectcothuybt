using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PRN221_GroupProject.Models;

namespace PRN221_GroupProject.Pages.View
{
    public class PaymentModel : PageModel
    {
        private readonly PRN221Context _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PaymentModel(PRN221Context context, IHttpContextAccessor httpContextAccessor)
        {
            _context= context;
            _httpContextAccessor= httpContextAccessor;
        }
        public IActionResult OnGet(int id)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            session.SetInt32("courseid", id);
            return Page();
        }
        public IActionResult OnPost(int id)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            MyCourse course = new MyCourse();
            course.UserId = (int)session.GetInt32("userid");
            course.CourseId = id;
            _context.MyCourses.Add(course);
            _context.SaveChanges();
            return RedirectToPage("/Index");
        }
    }
}
